import os
from os.path import expanduser
from breathPhaseSeparatorSVC import breathPhaseSeparatorSVC
from breathPhaseSeparatorFVC import breathPhaseSeparatorFVC

from scikits.audiolab import wavread
import numpy as np
import matplotlib.pyplot as plt


def readAudio(inputPath):
    # Read wav file
    inData, sampleRate, encoding = wavread(inputPath)

    # Stereo to mono conversion
    if len(inData.shape) == 2:
        # Split stereo audio/flow channels
        audioData = inData[:, 0]
        flowData = inData[:, 1]

        # Mixdown stereo channel
        # audioData = np.mean(audioData, axis=1, dtype=audioData.dtype)

    # Normalize data
    audioData /= np.max(audioData)
    flowData /= np.max(flowData)

    # plt.plot(audioData)
    # plt.plot(flowData)
    # plt.show()

    return [audioData, sampleRate, flowData]


########################### MAIN RUN
# base path of the training database (folder with wav files expected)
# basePath = expanduser("~") + '/Dropbox/BreathResearch/Mayo/BRI/svc_dvad/'
# basePath = expanduser("~") + '/Desktop/spiro/audio/'
# basePath = expanduser("~") + '/Desktop/BRI/svc/'
# basePath = expanduser("~") + '/Desktop/spiroFVC/'

basePath = expanduser("~") + '/Dropbox/ANNData/SpirometerRecordings/FVC/'





# get file list
fileList = os.listdir(basePath)
fileList.sort()

for currentFile in fileList:
    if 'wav' in str(currentFile) and 'png' not in str(currentFile):
        currentWorkingFile = basePath + currentFile
        print ('******************************************************************************************************')
        print('--------> Current Working file : ' + currentWorkingFile)

        # run specific session only (comment out for full)
        if 'Anita_light_FVC' not in currentWorkingFile: continue

        feedPureSessionPath = False

        if 'FVC' in basePath:
            print ('-> Running FVC branch...')
            [audioData, sampleRate, flowData] = readAudio(currentWorkingFile)
            BPS = breathPhaseSeparatorFVC(currentWorkingFile, audioData, sampleRate, flowData)
        else:
            print ('-> Running SVC branch...')
            if feedPureSessionPath:
                BPS = breathPhaseSeparatorSVC(currentWorkingFile)
            else:
                [audioData, sampleRate, flowData] = readAudio(currentWorkingFile)
                BPS = breathPhaseSeparatorSVC(currentWorkingFile, audioData, sampleRate, flowData)

        outputPhases = BPS.runAnalysis()

        # print (outputPhases)



