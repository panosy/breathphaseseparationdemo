# FVC Breath Phase initial Detector/Separator Class for NN parser incorporation (intermediate)
#
# Breath Research Inc.
#
# Panagiotis Giotis
#
# Includes both Time-Domain and Dynamic detection methods combined
# Dynamic method used VAD along with double multi-pass (for threshold and zone number detection) to locate
#   optimal candidate.
#
# -> Usage: separatorInstance = breathPhaseSeparatorFVC(audioFileFullPath)
#           outputDict = separatorInstance.runAnalysis()
#
# -> Expected input is either:
#      A full path+filename to an audio file (wav) containing one full breath cycle OR
#      A set of [sessionName, audioData, sampleRate, flowData] for server usage where audioVec/flow is already available
#
# -> Output is a dict with the following entries (all in seconds except final control flag):
#
# outDict["inhEndMarker"]
# outDict["exhStartMarker"]
# outDict["exhEndMarker"]
# outDict["silenceDurationAfterInh"]
# outDict["silenceDurationAfterExh"]
# outDict["secondPassFlag"]

# VERSION: FVC BETA 1.23
# Revision required for step inh end adjustments and hard limits.

import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
import time
from scikits.audiolab import wavread


class breathPhaseSeparatorFVC():

    def __init__(self, sessionName, audioInput = None, sampleRate = None, flowData = None):

        # Version for checking
        self.breathPhaseSeparatorVersion = 'FVC Beta 1.23'

        # Server deployment flag. Set True for use with afp, disables debug prints, enables timeStamp compensation
        self.afpDeployment = False

        # Control flags
        self.plotResultsWrite = False if self.afpDeployment else True
        self.printDebug = False if self.afpDeployment else True

        # Debug info for minor sliding adjustments
        self.printDebug2 = False

        # Generic LP flag
        self.pureLPsignal = False

        # Compensation for initial silence removal
        self.removeInitialSilence = True
        self.timeStampCompensation = 0.0

        # Check to read audio from file or get directly from args
        if audioInput is None:
            # Read in audio file and set parameters
            self.currentWorkingFile = sessionName
            # Read audio from file and store in self.audioData
            self.readAudio(self.currentWorkingFile)
        else:
            self.currentWorkingFile = sessionName
            self.audioData = audioInput
            self.sampleRate = sampleRate
            self.flowData = flowData

        # Audio parameters
        self.windowSize = 4096  # 5600# 4096  # inWindowSize (0.128 * 44100, try 0.096)
        self.hopSizeDen = 8
        self.hopSize = int(round(float(self.windowSize) / self.hopSizeDen))  # 512  # inHopSize
        self.numFramesPerBlock = len(self.audioData) / self.hopSize
        self.frameDurationInSecs = float(self.windowSize) / float(self.sampleRate)
        self.maskSize = 31  # try 21-31 was 31

        # Dynamic detection parameters
        self.signalWindow = 0.6  # 0.5 half a second and 0.8 --> 0.5 lower is stricter-->0.6
        self.threshold = 0.5  # 0.6 60% of energy in band --> 0.1 and 0.08 0.4 also! --> 0.45
        self.signalStartBand = 70  # 10 --> 60 / 50
        self.signalEndBand = 330  # 300 --> 330

        # Envelope
        self.envelope = []
        self.envelopeSmooth = []
        self.envelopeDer = []
        self.envelopeLength = 0
        # For easy index conversion time 2 envelope
        self.time2envelopeIndexMultiplier = 0
        self.exhEndTD = 0

        # Output phases dict for both methods
        self.breathPhasesDictTD = dict()
        self.breathPhasesDictDyn = dict()

        # TD for output
        self.numberOfZonesUsed = 0
        self.thresholdUsed = 0


    def readAudio(self, inputPath):
        # Read wav file
        self.audioData, self.sampleRate, encoding = wavread(inputPath)

        # Stereo to mono conversion
        if len(self.audioData.shape) == 2:
            self.audioData = np.mean(self.audioData, axis=1, dtype=self.audioData.dtype)
            self.channels = 1

        # Normalize data
        self.audioData /= np.max(self.audioData)


    def removeInitialSilenceFunction(self):
        if self.removeInitialSilence:
            print ('* Original session length before silence trimming: ' + str(float(len(self.audioData)) /
                                                                               self.sampleRate) + ' secs')

            self.totalTime = round(float(len(self.audioData)) / float(self.sampleRate), 3)
            envelope = self.getEnvelope(11)

            i = 0
            while envelope[i] < 0.05:  # was 0.04
                # print (abs(self.envelope[i]))
                i += 1
                if i > 0.35 * len(envelope):
                    print ('* Silent file breaking at 0.4..')
                    break

            percentage = float(i) / len(envelope)
            print (percentage)

            # convert envelope entry to audio
            i = int(np.floor(percentage * len(self.audioData)))

            # plt.plot(self.audioData)
            # plt.show()

            self.timeStampCompensation = float(i) / self.sampleRate

            self.audioData = self.audioData[i:-1]

            print ('* Final length: ' + str(float(len(self.audioData)) / self.sampleRate))

            # if self.printDebug:
            if True:
                print ('* Silence removal function removing ' + str(i) + ' samples, with total duration ' +
                       str(self.timeStampCompensation) + ' secs...')
                print ('* Final session length after silence trimming: ' + str(
                    float(len(self.audioData)) / self.sampleRate) + str(' secs'))


    def runAnalysis(self):


        # Remove initial silence chunk
        if self.removeInitialSilence:
            self.removeInitialSilenceFunction()

        # Get current total time (taking into account silence trimming)
        self.totalTime = round(float(len(self.audioData)) / float(self.sampleRate), 3)

        # Get audio envelope, 2 versions HQ and LQ
        self.envelope = self.getEnvelope(03)
        self.envelopeSmooth = self.getEnvelope(41)

        # Run original TD method (also for ExhEnd)
        self.detectBreathPhasesTD()

        # Export TD results in plots
        # self.plotResults(self.audioData, self.envelope, self.breathPhasesDictTD, self.sampleRate, 'td', str(len(signalBuckets)), minThres)

        # Get timer to calculate elapsed time
        start = time.time()

        # Run dynamic method
        self.detectBreathPhasesDyn()
        end = time.time()
        analysisTime = end - start

        # Export Dyn method results in plots (same working dir, same filenames)
        if self.plotResultsWrite:
            self.plotResults(self.audioData, self.envelope, self.breathPhasesDictDyn, self.sampleRate, 'dynamic',
                             self.numberOfZonesUsed, self.thresholdUsed, analysisTime)

        return self.breathPhasesDictDyn

    def powerList(self, inputList, power):
        return [x ** power for x in inputList]

    def detectBreathPhasesDyn(self):

        # Parameter initialization
        self.threshold = 0.1 # was 0.1
        minThres = 0.25
        minZones = 10
        minInhEnd = 0
        candidatePending = True
        acceptedZonesLimitL = 5 # 6
        acceptedZonesLimitR = 9 # 10

        self.exhEndTD = float(self.breathPhasesDictTD["exhEndMarker"] - 1) / len(self.envelope) * self.totalTime

        ##########################################################################################
        # Main while for candidate selection
        ##########################################################################################
        if self.printDebug:
            print ('--> Running Dynamic Zone extraction...')

        while self.threshold < 0.6 and candidatePending:
            self.threshold = self.threshold + 0.04  # 0.02
            rawDetection = self.detectSignalPresence()
            signalBuckets = self.convertBucketsToTime(rawDetection)

            # limit inhEnd -> inh duration can't be less than 0.8
            if len(signalBuckets) > 0:
                minInhEnd = signalBuckets[0]['signalStop']

            zones = len(signalBuckets)

            # minInhEnd <= 0.6 or minInhEnd >= 0.3 * totalTime
            if self.printDebug:
                print ('*** Increasing Dyn threshold to: ' + str(self.threshold) + ', getting ' + str(zones) +
                       ' zones. Current inh end: ' + str(minInhEnd))

            if self.printDebug:
                print ('* Detected signal buckets: ' + str(signalBuckets))

            # CHECK FOR CANDIDATE:
            # print (zones <= minZones)
            # print (zones >= acceptedZonesLimitL)
            # print (zones <= acceptedZonesLimitR)
            # print (minInhEnd < 0.35 * self.totalTime)
            # print (minInhEnd >= 0.4)

            # Check specs for possible candidate <-double check!
            if zones <= minZones and zones >= acceptedZonesLimitL and zones <= acceptedZonesLimitR and \
                    minInhEnd < 0.35 * self.totalTime and minInhEnd >= 0.4:

                minZones = zones
                minThres = self.threshold
                candidatePending = False
                if self.printDebug: print ('=*=*=> Candidate found! Setting optimal thres: ' + str(
                    self.threshold) + ', producing ' + str(zones) + ' zones')

            if self.threshold >= 0.6 and candidatePending is True:
                self.threshold = 0.1
                if acceptedZonesLimitR <= 14: # 10
                    acceptedZonesLimitR += 1 # 1
                else:
                    # Check for never-ending loop (bad audio or unexpected behavior)
                    print ('***** Error, no candidate found, potential bad audio input!! ****')
                    break

                if acceptedZonesLimitL > 2:
                    acceptedZonesLimitL -= 1

                if self.printDebug: print ('************** [Multi-pass] Expanding zone range: ' + str(
                    acceptedZonesLimitL) + ' - ' + str(acceptedZonesLimitR))

                continue

        if candidatePending == True:
            print ('*** Error, can not find any separation zones for session ' + str(
                self.currentWorkingFile) + ', exiting..')
            exit(0)

        ################################################################################################################
        #################################  Main run for optimal detected threshold  ####################################
        ################################################################################################################
        self.threshold = minThres
        rawDetection = self.detectSignalPresence()
        signalBuckets = self.convertBucketsToTime(rawDetection)

        self.thresholdUsed = minThres
        self.numberOfZonesUsed = len(signalBuckets)

        print ('*|*|* --> Running Dynamic Detection with threshold: ' + str(self.thresholdUsed) + ', zones: ' + str(
            self.numberOfZonesUsed))

        # Print out final results
        if self.printDebug:
            print signalBuckets

        # ------------------------------------------------------
        # Inh End
        # start from 0 bucket
        i = 0
        if self.printDebug: print ('******************* Dynamic Inh End Point Extraction...')
        # print (self.envelope[int(round(signalBuckets[i]['signalStop'] * self.time2envelopeIndexMultiplier))])

        # ------------------------------------------------------
        # Get Inhalation bucket -> was at 0.07 FOR GOOD RUN!!

        bucketDuration = signalBuckets[i+1]['signalStop'] - signalBuckets[i]['signalStop']

        self.envelopeDer2[self.time2EnvelopeIndex(signalBuckets[i]['signalStop'])]

        ######################################################################
        ## ZERO CROSSINGS OF DERIVATIVE
        movAvFilter = (1.0 / 21) * np.ones(21)
        self.envelopeDer = np.convolve(movAvFilter, self.envelopeDer, mode='same')
        self.envelopeDer /= max(self.envelopeDer)
        zeroCrossings = np.where(np.diff(np.sign(self.envelopeDer)))[0]
        zeroCrossingsTime = np.dot(zeroCrossings, 1 / self.time2envelopeIndexMultiplier)
        # print (zeroCrossings)
        self.inhEndEstimate = zeroCrossingsTime[1]
        j = 1
        while self.envelopeAtTime(self.inhEndEstimate) > 0.5:
            j += 1
            self.inhEndEstimate = zeroCrossingsTime[j]
        print ('->> InhEndEstimate: ' + str(self.inhEndEstimate))

        ##########################################################################

        if self.printDebug:
            print ('* Getting INH bucket')
            print (self.envelopeAtTime(signalBuckets[i]['signalStop']) > 0.04)
            print (signalBuckets[i + 1]['signalStop'] < 0.45 * self.totalTime)
            # print (self.envelopeAtTime(signalBuckets[i+1]['signalStop']) < 0.2)
            print (i < len(signalBuckets) - 2)
            print(bucketDuration < 0.2*self.totalTime)
            print (signalBuckets[i]['signalStop'] < self.exhEndTD)
            print (signalBuckets[i + 1]['signalStop'] < self.exhEndTD)
            print (signalBuckets[i + 2]['signalStop'] < self.exhEndTD)

        subEnvelope = self.envelope[self.time2EnvelopeIndex(signalBuckets[i]['signalStop']) : self.time2EnvelopeIndex(signalBuckets[i + 1]['signalStop']) ]
        # FIRST LINE SIGNAL STOP > 0.07 | limit 0.3 * totalTime

        print ('*********** ' + str(signalBuckets[i + 2]['signalStop']))
        print (self.exhEndTD)

        while self.envelopeAtTime(signalBuckets[i]['signalStop']) > 0.04 and \
                signalBuckets[i + 1]['signalStop'] < 0.45 * self.totalTime and \
                i < len(signalBuckets) - 2 and \
                self.envelopeAtTime(signalBuckets[i]['signalStop']) >= self.envelopeAtTime(signalBuckets[i+1]['signalStop']) and \
                signalBuckets[i]['signalStop'] < self.exhEndTD and \
                signalBuckets[i + 1]['signalStop'] < self.exhEndTD and \
                signalBuckets[i + 2]['signalStop'] < self.exhEndTD and \
                abs(signalBuckets[i]['signalStop'] - self.inhEndEstimate) > abs(signalBuckets[i+1]['signalStop'] - self.inhEndEstimate):

            if self.printDebug: print ('Shifting Inh bucket, setting at: ' + str(signalBuckets[i+1]['signalStop']))
            # Shift to right-most bucket
            i += 1
            bucketDuration = signalBuckets[i+1]['signalStop'] - signalBuckets[i]['signalStop']

            if self.printDebug:
                print ('* Getting INH bucket')
                print (self.envelopeAtTime(signalBuckets[i]['signalStop']) > 0.04)
                print (signalBuckets[i + 1]['signalStop'] < 0.45 * self.totalTime)
                print (i < len(signalBuckets) - 2)
                print (bucketDuration < 0.2*self.totalTime)
                # print (signalBuckets[i+2]['signalStop'] < self.exhEndTD)
                print (self.envelopeAtTime(signalBuckets[i]['signalStop']) >= self.envelopeAtTime(signalBuckets[i+1]['signalStop']))
                print (signalBuckets[i+1]['signalStop'] < self.exhEndTD)
                print (self.envelopeDer[self.time2EnvelopeIndex(signalBuckets[i]['signalStop'])])

        # Set the appropriate bucket end as inhEnd
        self.breathPhasesDictDyn["inhEndMarker"] = float(signalBuckets[i]['signalStop'])

        if self.printDebug: print (
                    'Original InhEnd point before rectification: ' + str(self.breathPhasesDictDyn["inhEndMarker"]))

        # ------------------------------------------------------
        ## SHIFT RIGHT
        # Set threshold zone based on max of inhalation zone
        inhMaxPeak = np.max(self.envelope[0: self.time2EnvelopeIndex(self.breathPhasesDictDyn["inhEndMarker"])])

        # WAS 0.005 * inhMaxPeak
        if inhMaxPeak < 0.2:
            inhMaxPeak = 0.2

        if inhMaxPeak > 0.7:
            inhThres = 0.024
        elif 0.7 >= inhMaxPeak > 0.4:
            inhThres = 0.022
        else:
            inhThres = 0.02

        while self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]) >= inhThres \
                and self.breathPhasesDictDyn["inhEndMarker"] <= signalBuckets[i + 1]['signalStart'] \
                and self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"] + 0.004) < \
                self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]) and \
                self.breathPhasesDictDyn["inhEndMarker"] < self.exhEndTD:

            self.breathPhasesDictDyn["inhEndMarker"] += 0.004

            if self.printDebug2:
                print ('* Sliding inhEnd RIGHT to: ' + str(self.breathPhasesDictDyn["inhEndMarker"]) + ' ,envVal: ' \
                + str(self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"])))
                # print ('* threshold: ' + str(inhThres))

        # Add margin
        # self.breathPhasesDictDyn["inhEndMarker"] += 0.01

        print ('env1: ' + str(self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"])))
        print ('env2: ' + str(self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"] - 0.01)))

        print (self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"] - 0.005) < 0.015)
        print (self.breathPhasesDictDyn["inhEndMarker"] - 0.005 > 0.65)
        print (self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]) >= self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]-0.01))

        ######### SHIFT LEFT
        while self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"] - 0.005) < 0.015 and \
                self.breathPhasesDictDyn["inhEndMarker"] - 0.005 > 0.65\
                and self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]) >= self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]-0.01):

            # print ('env1: ' + str(self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"])))
            # print ('env2: ' + str(self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"] - 0.005)))

            self.breathPhasesDictDyn["inhEndMarker"] -= 0.005
            if self.printDebug: print ('* Sliding inhEnd LEFT according to envelope value, to: ' + str(
                self.breathPhasesDictDyn["inhEndMarker"]))

        ###### NEW ON DER
        # while self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]) > 0.05 and \
        #     self.envelopeDer[self.time2EnvelopeIndex(self.breathPhasesDictDyn["inhEndMarker"])] >= 0:


        # while self.envelopeDer[self.time2EnvelopeIndex(self.breathPhasesDictDyn["inhEndMarker"])] >= 0:

        while self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]) > 0.05 and \
                self.envelopeDer[self.time2EnvelopeIndex(self.breathPhasesDictDyn["inhEndMarker"])] >= 0:

                # self.envelopeDer[self.time2EnvelopeIndex(self.breathPhasesDictDyn["inhEndMarker"])] > self.envelopeDer[
            # self.time2EnvelopeIndex(self.breathPhasesDictDyn["inhEndMarker"] - 0.01)]:
            print ('* SHIFTING INH LEFT BASED ON DERIVATIVE')
            self.breathPhasesDictDyn["inhEndMarker"] -= 0.01
            print (self.envelopeDer[self.time2EnvelopeIndex(self.breathPhasesDictDyn["inhEndMarker"])])



        while self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]) > 0.05 and \
                self.envelopeDer[self.time2EnvelopeIndex(self.breathPhasesDictDyn["inhEndMarker"])] <= 0:
            #
            print ('* SHIFTING INH RIGHT BASED ON DERIVATIVE')
            print (self.envelopeDer[self.time2EnvelopeIndex(self.breathPhasesDictDyn["inhEndMarker"])])
            self.breathPhasesDictDyn["inhEndMarker"] += 0.01

        self.breathPhasesDictDyn["inhEndMarker"] -= 0.04

        # ------------------------------------------------------
        # ALTERNATIVE APPROACH FOR INH LEFT SHIFT  ######## EEEEEXPERIMENTAL
        xx = 0.7
        if i > 0:
            xx = signalBuckets[i-1]['signalStop']

        # print(self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"] - 0.012) < 2.1*inhThres)
        # print(self.breathPhasesDictDyn["inhEndMarker"] > xx)

        while self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"] - 0.012) < 1.2*inhThres and \
                self.breathPhasesDictDyn["inhEndMarker"] > xx: #and \
                # self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"]) - \
                # self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"] - 0.012) > 0.01:  # try with > inhalation end

            if self.printDebug:
                # print ('* Current exh start: ' + str(self.breathPhasesDictDyn["exhStartMarker"]))
                print ('* Shift LEFT InhEnd ALT2 to: ' + str(self.breathPhasesDictDyn["inhEndMarker"]) +
                       ', envVal: ' + str(self.envelopeAtTime(self.breathPhasesDictDyn["inhEndMarker"])))

            self.breathPhasesDictDyn["inhEndMarker"] -= 0.004

            # print ('Rectified InhEnd point: ' + str(self.breathPhasesDictDyn["inhEndMarker"]))

        self.breathPhasesDictDyn["inhEndMarker"] += 0.08 # WAS 0.04

        # ------------------------------------------------------
        # EXH START
        # go to next bucket for exh
        while self.breathPhasesDictDyn["inhEndMarker"] > signalBuckets[i]['signalStart'] and i < len(signalBuckets)-1:
            i = i + 1

        if len(signalBuckets) > 2:
            val = self.envelope[int(round(signalBuckets[i]['signalStop'] * self.time2envelopeIndexMultiplier))]  # i+1 !
        else:
            val = 1

        if self.printDebug: print ('******************* Dynamic Exh Start Point Extraction...')
        # Parse for Exhalation Start
        if i <= len(signalBuckets) - 1:

            # val 0.1
            # While current bucket end is under threshold (not inside exhalation) and events are not very far apart
            # and the area defined by the bucket doesn't have high values. Dealing with close multiple bucket within
            # the pause zone

            # print ('Start point: ' + str(int(round(signalBuckets[i]['signalStart'] * time2envelopeIndexMultiplier))))
            # print ('End point: ' + str(int(round(signalBuckets[i]['signalStop'] * time2envelopeIndexMultiplier))))
            # print ('envelope: ' + str(envelope[int(round(signalBuckets[i]['signalStart'] * time2envelopeIndexMultiplier)):
            #                         int(round(signalBuckets[i]['signalStop'] * time2envelopeIndexMultiplier))]))

            while val < 0.05 and i < len(signalBuckets) - 1 and \
                    (signalBuckets[i]['signalStop'] - signalBuckets[i]['signalStart']) <= 2 and \
                    np.max(
                        self.envelope[int(round(signalBuckets[i]['signalStart'] * self.time2envelopeIndexMultiplier)):
                        int(round(signalBuckets[i]['signalStop'] * self.time2envelopeIndexMultiplier))]) < 0.2 and \
                    signalBuckets[i+1]['signalStop'] < self.exhEndTD:

                if self.printDebug: print ('Skipping exh with end value: ' + str(val) + 'at time ' +
                                           str(signalBuckets[i]['signalStart']) + ', in bucket i: ' + str(i))
                i += 1

                if i == len(signalBuckets) - 1:  # Check for out of bounds (last bucket)
                    break
                else:
                    val = self.envelope[int(round(signalBuckets[i]['signalStop'] * self.time2envelopeIndexMultiplier))]  # +1 and signalStart

        self.breathPhasesDictDyn["exhStartMarker"] = float(signalBuckets[i]['signalStart'])

        # Get max point of exhalation
        maxExhPeak = np.max(self.envelope[self.time2EnvelopeIndex(self.breathPhasesDictDyn["exhStartMarker"]): -2])

        while self.envelopeAtTime(self.breathPhasesDictDyn["exhStartMarker"]) > 0.04 * maxExhPeak and \
                self.breathPhasesDictDyn["exhStartMarker"] > self.breathPhasesDictDyn["inhEndMarker"] + 0.08:
            if self.printDebug: print ('* Shifting exhStart to the left')
            self.breathPhasesDictDyn["exhStartMarker"] -= 0.005

        # ------------------------------------------------------
        # SHIFT RIGHT ### 0.01 /// WAS 0.013 ->>0.02*maxPeak
        while self.envelopeAtTime(self.breathPhasesDictDyn["exhStartMarker"] + 0.004) < 0.025 * maxExhPeak and \
                self.breathPhasesDictDyn["exhStartMarker"] > signalBuckets[i - 1]['signalStop'] and \
                self.breathPhasesDictDyn["exhStartMarker"] < self.exhEndTD:  # try with > inhalation end

            if self.printDebug:
                print ('* Sliding RIGHT exh start -> envVal: ' + str(self.envelopeAtTime(self.breathPhasesDictDyn["exhStartMarker"])) \
                       + '. Current exh start: ' + str(round(self.breathPhasesDictDyn["exhStartMarker"], 3)))
            # Shift right
            self.breathPhasesDictDyn["exhStartMarker"] += 0.004

        # ------------------------------------------------------
        # # NEW DOUBLE CHECK - > shift left
        # while envelope[int(round(breathPhasesDict2["exhStartMarker"] * time2envelopeIndexMultiplier))] > 0.05 and \
        #         breathPhasesDict2["exhStartMarker"] > signalBuckets[i-1]['signalStop']:  # try with > inhalation end
        #     print ('* Sliding LEFT exh start according to envelope value...: ' + str(round(envelope[int(round(breathPhasesDict2["exhStartMarker"] * time2envelopeIndexMultiplier))])))
        #     breathPhasesDict2["exhStartMarker"] -= 0.008

        # get edge compensation
        if self.breathPhasesDictDyn["exhStartMarker"] - 0.035 > self.breathPhasesDictDyn["inhEndMarker"]:
            self.breathPhasesDictDyn["exhStartMarker"] -= 0.035

        # rectify overlap (extreme exception, 1 invalid session in Mayo set)
        if self.breathPhasesDictDyn["exhStartMarker"] <= self.breathPhasesDictDyn["inhEndMarker"]:
            self.breathPhasesDictDyn["exhStartMarker"] = self.breathPhasesDictDyn["inhEndMarker"] + 0.03


        # ------------------------------------------------------
        # Get from Td
        self.breathPhasesDictDyn["exhEndMarker"] = float(self.breathPhasesDictTD["exhEndMarker"] - 1) / len(
            self.envelope) * self.totalTime

        maxExhPeak = np.max(self.envelope[int(
            round(self.breathPhasesDictDyn["exhStartMarker"] * self.time2envelopeIndexMultiplier)): len(self.envelope)])

        while self.envelope[int(round(
                self.breathPhasesDictDyn["exhEndMarker"] * self.time2envelopeIndexMultiplier))] < 0.028 * maxExhPeak \
                and self.breathPhasesDictDyn["exhEndMarker"] > 0.6 * self.totalTime:
            self.breathPhasesDictDyn["exhEndMarker"] -= 0.02

        if self.breathPhasesDictDyn["exhEndMarker"] + 0.07 <= self.totalTime:
            self.breathPhasesDictDyn["exhEndMarker"] += 0.06

        # ------------------------------------------------------
        # Compensate for initial silence removal:
        if self.removeInitialSilence and self.afpDeployment:
            self.breathPhasesDictDyn["inhEndMarker"] += self.timeStampCompensation
            self.breathPhasesDictDyn["exhStartMarker"] += self.timeStampCompensation
            self.breathPhasesDictDyn["exhStartMarker"] += self.timeStampCompensation

        # Output final separation Markers
        print ('inhEndMarker = ' + str(self.breathPhasesDictDyn["inhEndMarker"]))
        print ('exhStartMarker = ' + str(self.breathPhasesDictDyn["exhStartMarker"]))
        print ('exhEndMarker = ' + str(self.breathPhasesDictDyn["exhEndMarker"]))

        # Calculate/store pause durations
        self.breathPhasesDictDyn["silenceDurationAfterInh"] = self.breathPhasesDictDyn["exhStartMarker"] - \
                                                              self.breathPhasesDictDyn[
                                                                  "inhEndMarker"]
        self.breathPhasesDictDyn["silenceDurationAfterExh"] = self.totalTime - self.breathPhasesDictDyn["exhEndMarker"]
        self.breathPhasesDictDyn["secondPassFlag"] = 'dynVAD+TD'

    # Envelope extraction
    def getEnvelope(self, maskSize):

        audioData = self.audioData.copy()
        # Low pass data
        # cutoff = 600.0 / (0.5 * self.sampleRate)  # 450
        cutoff = self.signalEndBand / (0.5 * self.sampleRate)  # 450
        b, a = signal.butter(2, cutoff, 'low', analog=False)
        audioDataFiltered = signal.filtfilt(b, a, audioData)
        audioData = audioDataFiltered
        audioData /= np.max(audioData)

        # Copy LP version to main audio to use for analysis (False)
        if self.pureLPsignal:
            self.audioData = audioData

        pin = 0
        pend = len(audioData) - self.windowSize
        count = 0
        envelope = np.zeros(self.numFramesPerBlock)

        while pin < pend:
            buffer = self.audioData[pin: pin + self.windowSize] * self.windowSize
            envelope[count] = np.sum(np.abs(buffer))
            pin += self.hopSize
            count += 1

        # normalize envelope according to max value
        envelope /= max(envelope)
        # moving average
        movAvFilter = (1.0 / maskSize) * np.ones(maskSize)

        envelopeSmooth = np.convolve(movAvFilter, envelope, mode='same')
        envelopeSmooth /= (max(envelopeSmooth) + 10e-4)

        envelope = envelopeSmooth

        # self.envelope = envelope
        self.envelopeDer = np.diff(envelope)
        # self.envelopeDer = np.insert(self.envelopeDer, 0, 0)
        self.envelopeDer = np.append(self.envelopeDer, 0)
        self.envelopeDer /= np.max(self.envelopeDer)

        self.envelopeDer2 = np.diff(self.envelopeDer)
        self.envelopeDer2 = np.append(self.envelopeDer2, 0)
        # self.envelopeDer2 = np.insert(self.envelopeDer2, 0, 0)
        self.envelopeDer2 /= np.max(self.envelopeDer2)

        self.envelopeLength = len(envelope)
        self.time2envelopeIndexMultiplier = float(self.envelopeLength) / self.totalTime

        return envelope

    # Time-domain method
    def detectBreathPhasesTD(self):

        self.envelopeLength = len(self.envelope)

        silenceVec = self.envelope.copy()
        SVCsessionDurationSecs = round(float(len(self.audioData)) / float(self.sampleRate), 3)

        # zero elements under threshold
        silenceIndices = silenceVec < 0.07  # FUNCTIONAL AT 0.02
        silenceVec[silenceIndices] = 0
        # inhEndMarker = next((i for i, x in enumerate(limitIDX) if x), None)
        limitIDX = [i for i, e in enumerate(silenceVec) if e != 0]

        # Avoid mis-cut files (silence in the beginning)
        j = int(0.07 * len(silenceVec))

        # Search for first marker (inh end)
        while silenceVec[j] > 0 and j < len(silenceVec) - 1: j += 1
        inhEndMarker = j
        # Search for second marker (exh start)
        while silenceVec[j] == 0 and j < len(silenceVec) - 1: j += 1
        exhStartMarker = j
        exhEndMarker = limitIDX[-1]  # get end pause start

        secondPassFlag = False

        # Check for second pass if limits are very close to one-another (misdetection)
        if abs(exhEndMarker - exhStartMarker) < 5:
            print ("Increasing threshold for session!")
            # zero elements under threshold
            silenceIndices = silenceVec < 0.05
            silenceVec[silenceIndices] = 0
            limitIDX = [i for i, e in enumerate(silenceVec) if e != 0]

            j = int(0.1 * len(silenceVec))
            while silenceVec[j] > 0 and j < len(silenceVec) - 1:
                j += 1
            inhEndMarker = j + 1
            while silenceVec[j] == 0 and j < len(silenceVec) - 1:
                j += 1
            exhStartMarker = j + 1
            exhEndMarker = limitIDX[-1]  # get end pause start
            secondPassFlag = True

        silenceDurationAfterInh = round(
            ((float(exhStartMarker - inhEndMarker) / self.envelopeLength) * SVCsessionDurationSecs), 3)
        silenceDurationAfterExh = round(((1 - float(exhEndMarker) / self.envelopeLength) * SVCsessionDurationSecs), 3)
        # pauseStartIdxMarker = limitIDX[-1] + 1

        breathPhasesDict = dict()
        breathPhasesDict["inhEndMarker"] = inhEndMarker
        breathPhasesDict["exhStartMarker"] = exhStartMarker
        breathPhasesDict["exhEndMarker"] = exhEndMarker
        breathPhasesDict["silenceDurationAfterInh"] = silenceDurationAfterInh
        breathPhasesDict["silenceDurationAfterExh"] = silenceDurationAfterExh
        breathPhasesDict["secondPassFlag"] = secondPassFlag

        self.breathPhasesDictTD = breathPhasesDict
        # print (breathPhasesDict)


    # Aux Functions

    def envelopeAtTime(self, timeStamp):
        return self.envelope[int(round(timeStamp * self.time2envelopeIndexMultiplier))]

    def time2EnvelopeIndex(self, timeStamp):
        return int(round(timeStamp * self.time2envelopeIndexMultiplier))

    def calculateEnergy(self, data):
        dataAmplitude = np.abs(np.fft.fft(data))
        dataAmplitude = dataAmplitude[1:]
        dataEnergy = dataAmplitude ** 2
        return dataEnergy

    def calculateNormalizedEnergy(self, data):
        dataFreq = np.fft.fftfreq(len(data), 1.0 / self.sampleRate)
        dataFreq = dataFreq[1:]
        dataEnergy = self.calculateEnergy(data)

        # Connect energy with frequencies
        energyFreq = {}
        for (i, freq) in enumerate(dataFreq):
            if abs(freq) not in energyFreq:
                energyFreq[abs(freq)] = dataEnergy[i] * 2

        return energyFreq

    def sumEnergyInBand(self, energyFrequencies, startBand, endBand):
        sumEnergy = 0
        for f in energyFrequencies.keys():
            if startBand < f < endBand:
                sumEnergy += energyFrequencies[f]
        return sumEnergy

    def medianFilter(self, x, k):
        assert k % 2 == 1, "Median filter length must be odd."
        assert x.ndim == 1, "Input must be one-dimensional."
        k2 = (k - 1) // 2
        y = np.zeros((len(x), k), dtype=x.dtype)
        y[:, k2] = x
        for i in range(k2):
            j = k2 - i
            y[j:, i] = x[:-j]
            y[:j, i] = x[0]
            y[:-j, -(i + 1)] = x[j:]
            y[-j:, -(i + 1)] = x[-1]
        return np.median(y, axis=1)

    def convertBucketsToTime(self, detectedWindows):
        signalTime = []
        isSignal = 0
        for window in detectedWindows:

            if (window[1] == 1.0 and isSignal == 0):
                isSignal = 1
                signalLabel = {}
                signalTimeStart = window[0] / self.sampleRate
                signalLabel['signalStart'] = signalTimeStart
                # print(window[0], signalTimeStart)
                # signalTime.append(signalLabel)

            if (window[1] == 0.0 and isSignal == 1):
                isSignal = 0
                signalTimeEnd = window[0] / self.sampleRate

                # check for very short inh case and skip
                # if signalTimeEnd < 0.1*self.exhEndTD: # was 0.65 fix
                if signalTimeEnd < 0.3:
                    print ('Skipping bucket inside VAD!')
                    continue

                signalLabel['signalStop'] = signalTimeEnd
                signalTime.append(signalLabel)
        return signalTime

    def detectSignalPresence(self):
        # Detects signal regions based on ratio between signal band energy and total energy.
        # Output is array of window numbers and signal flags (1 - signal, 0 - nonsignal).

        detectedWindows = np.array([])
        windowSize = self.windowSize
        hopSize = self.hopSize

        data = self.audioData
        # data = self.inputData
        sampleStart = 0

        while sampleStart < (len(data) - windowSize):
            sampleEnd = sampleStart + windowSize
            if sampleEnd >= len(data):
                sampleEnd = len(data) - 1
            dataWindow = data[sampleStart:sampleEnd]
            energyFreq = self.calculateNormalizedEnergy(dataWindow)
            sumSignalEnergy = self.sumEnergyInBand(energyFreq, self.signalStartBand, self.signalEndBand)
            sumFullEnergy = sum(energyFreq.values())
            signalRatio = sumSignalEnergy / sumFullEnergy

            # When there is a signal sequence we have ratio of energies more than Threshold
            signalRatio = signalRatio > self.threshold
            detectedWindows = np.append(detectedWindows, [sampleStart, signalRatio])
            sampleStart += hopSize

        detectedWindows = detectedWindows.reshape(int(len(detectedWindows) / 2), 2)

        # Smooth detection
        medianWindow = int(float(self.signalWindow) / (float(self.windowSize) / self.sampleRate))
        if medianWindow % 2 == 0:
            medianWindow = medianWindow - 1
        medianEnergy = self.medianFilter(detectedWindows[:, 1], medianWindow)
        detectedWindows[:, 1] = medianEnergy

        return detectedWindows


    def plotResults(self, audioData, envelope, breathPhasesDict, sampleRate, method, zones, threshold, analysisTime):

        end = round(float(len(audioData)) / float(sampleRate), 3)
        timeAxis = np.round(np.linspace(0, end, len(envelope), endpoint=False), 3)

        plt.plot(timeAxis, envelope, 'black', label='Breath flow')

        figTitle = str(self.currentWorkingFile)

        if method == 'dynamic' and not self.afpDeployment:
            breathPhasesDict["inhEndMarker"] = int(
                float(breathPhasesDict["inhEndMarker"]) / timeAxis[-1] * len(timeAxis))
            breathPhasesDict["exhStartMarker"] = int(
                float(breathPhasesDict["exhStartMarker"]) / timeAxis[-1] * len(timeAxis))
            breathPhasesDict["exhEndMarker"] = int(
                float(breathPhasesDict["exhEndMarker"]) / timeAxis[-1] * len(timeAxis))
            plt.title('FVC: ' + figTitle)

            self.inhEndEstimate = int( float(self.inhEndEstimate) / timeAxis[-1] * len(timeAxis))
        else:
            plt.title('Phase Separation (TD): ' + figTitle)

        plt.axvline(x=timeAxis[breathPhasesDict["inhEndMarker"]], color='red', linestyle='--', label='Inh end')
        plt.axvline(x=timeAxis[breathPhasesDict["exhStartMarker"]], color='green', linestyle='--', label='Exh start')
        plt.axvline(x=timeAxis[breathPhasesDict["exhEndMarker"]], color='blue', linestyle='--', label='Exh end')

        plt.text(0.7 * timeAxis[-1], 0.7, str('method: ' + str(breathPhasesDict["secondPassFlag"])),
                 horizontalalignment='left', verticalalignment='center')
        plt.text(0.7 * timeAxis[-1], 0.65,
                 str('silenceAfterInh(s): ' + str(round(breathPhasesDict["silenceDurationAfterInh"], 3))),
                 horizontalalignment='left', verticalalignment='center')
        plt.text(0.7 * timeAxis[-1], 0.6,
                 str('silenceAfterExh(s): ' + str(round(breathPhasesDict["silenceDurationAfterExh"], 3))),
                 horizontalalignment='left', verticalalignment='center')
        plt.text(0.7 * timeAxis[-1], 0.55, str('Zones used: ' + str(zones)), horizontalalignment='left',
                 verticalalignment='center')
        plt.text(0.7 * timeAxis[-1], 0.5, str('Threshold: ' + str(self.thresholdUsed)), horizontalalignment='left',
                 verticalalignment='center')
        plt.text(0.7 * timeAxis[-1], 0.45, str('Analysis time: ' + str(round(analysisTime, 2)) + ' s'), horizontalalignment='left',
                 verticalalignment='center')
        #
        plt.text(0.7 * timeAxis[-1], 0.4, str('CodeVersion: ' + str(self.breathPhaseSeparatorVersion)), horizontalalignment='left', verticalalignment='center')

        plt.xlabel('time(s)')
        plt.grid(True)
        plt.legend()
        # plt.savefig(str(self.currentWorkingFile) + '_' + str(self.hopSizeDen) + '.png')

        # plt.savefig(str(self.currentWorkingFile) + '_LP_' + str(self.signalEndBand) + '.png')
        plt.savefig(str(self.currentWorkingFile) + '.png')

        plt.close()

        #### SUBPLOT
        plt.subplot(3, 1, 1)
        plt.plot(timeAxis, self.envelope, 'black', label='EnvelopeSmooth')
        plt.axvline(x=timeAxis[breathPhasesDict["inhEndMarker"]], color='red', linestyle='--', label='Inh end')
        plt.axvline(x=timeAxis[breathPhasesDict["exhStartMarker"]], color='green', linestyle='--', label='Exh start')
        plt.axvline(x=timeAxis[breathPhasesDict["exhEndMarker"]], color='blue', linestyle='--', label='Exh end')
        plt.axvline(x=timeAxis[self.inhEndEstimate], color='magenta', linestyle='--', label='estInhEnd')
        plt.grid(True)
        plt.title('FVC EnvRaw')

        plt.subplot(3, 1, 2)
        plt.plot(timeAxis, self.envelopeSmooth, 'black', label='EnvelopeSmooth')
        plt.axvline(x=timeAxis[breathPhasesDict["inhEndMarker"]], color='red', linestyle='--', label='Inh end')
        plt.axvline(x=timeAxis[breathPhasesDict["exhStartMarker"]], color='green', linestyle='--', label='Exh start')
        plt.axvline(x=timeAxis[breathPhasesDict["exhEndMarker"]], color='blue', linestyle='--', label='Exh end')
        plt.grid(True)
        plt.title('FVC EnvSmooth')

        # print (len(timeAxis))
        # print (len(self.envelopeDer))
        plt.subplot(3, 1, 3)
        plt.plot(timeAxis, self.envelopeDer, 'black', label='EnvelopeSmooth')
        plt.axvline(x=timeAxis[breathPhasesDict["inhEndMarker"]], color='red', linestyle='--', label='Inh end')
        plt.axvline(x=timeAxis[breathPhasesDict["exhStartMarker"]], color='green', linestyle='--', label='Exh start')
        plt.axvline(x=timeAxis[breathPhasesDict["exhEndMarker"]], color='blue', linestyle='--', label='Exh end')
        plt.axvline(x=timeAxis[self.inhEndEstimate], color='magenta', linestyle='--', label='estInhEnd')

        plt.grid(True)
        plt.title('FVC EnvDerivative')

        # plt.subplot(4, 1, 4)
        # plt.plot(timeAxis, abs(self.envelopeDer2), 'black', label='EnvelopeSmooth')
        # plt.axvline(x=timeAxis[breathPhasesDict["inhEndMarker"]], color='red', linestyle='--', label='Inh end')
        # plt.axvline(x=timeAxis[breathPhasesDict["exhStartMarker"]], color='green', linestyle='--', label='Exh start')
        # plt.axvline(x=timeAxis[breathPhasesDict["exhEndMarker"]], color='blue', linestyle='--', label='Exh end')
        # plt.grid(True)
        # plt.title('EnvDer')

        plt.savefig(str(self.currentWorkingFile) + '_env.png')
        plt.close()

