Breath Research Inc.

Panagiotis Giotis

Breath Phase initial Detector/Separator Framework

To be used in conjuction with BRI's NN framework for pathology detection

The framework uses a combined approach where both Time-Domain and Dynamic detection methods are applied. The dynamic
approach makes use of a heavily-modified algorithmic equivalent of the VAD (Voice Activity Detection), custom-tailored
to fit the characteristics (spectral and otherwise) of the studied signals.

In addition a double-bound multi-pass technique is applied (both for threshold and zone number detection). This is
fundamental due to our inputs sensitivity to noise, wide range of characteristics and varying scenarios both of quality
and of exceptions of the recording conditions.

 -> Usage: separatorInstance = breathPhaseSeparatorFVC(audioFileFullPath)
           outputDict = separatorInstance.runAnalysis()

 -> Expected input is either:
      A full path+filename to an audio file (wav) containing one full breath cycle OR
      A set of [sessionName, audioData, sampleRate, flowData] for server usage where audioVec/flow is already available

 -> Output is a dict with the required entries, described in detail in the each protocols corresponding functions